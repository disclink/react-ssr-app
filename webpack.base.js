const webpack = require('webpack')
const define = require('./.define.json')

function stringify(input) {
  let output = {}
  for (var k in input) {
    if (input.hasOwnProperty(k)) {
      output[k] = JSON.stringify(input[k])
    }
  }
  return output
}

module.exports = {
  // Tell webpack to run babel on every file it runs through
  module: {
    rules: [
      {
        test: /\.js?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          presets: [
            '@babel/preset-react',
            ['@babel/env', { targets: { browsers: ['last 2 versions'] } }]
          ]
        }
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin(stringify(define))
  ]
};
